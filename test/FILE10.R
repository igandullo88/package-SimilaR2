## ###################################################################
## Ejercicios de vectores, factores y gráficos.
## ###################################################################

## Los datos necesarios para realizar los siguientes ejercicios se
## encuentran en el fichero repaso1.RData.

load("repaso1.RData")

## ###################################################################
## Ejercicio 1
## ###################################################################

## El vector telefonos contiene el número de líneas telefónicas en
## distintas zonas del mundo y distintos años. Los datos están
## organizados por zonas, y para cada zona, por años.

## Los vectores lugar y fecha contienen la información relativa a las
## zonas y años de los que se tiene información.

## ###################################################################
## (1) Crear un factor con la zona asociada a cada dato del vector
## telefonos, y otro factor con la fecha.

factor_zona=factor(rep(lugar, each= 7))
factor_zona

# La función "factor" nos ayuda a transformar un vector a un factor, diferenciando en el los distintos niveles que tenga, que seren los mismos que el número de datos distintos en el mismo..
# La función "rep" la usamos para repetir un dato o varios datos el número de veces que se le indique. 
# El argumento "each" de la función "rep" lo usamos para repetir cada dato de un vector el número de veces que se le indique. 

factor_año=factor(rep(fecha, times= 7))
factor_año

# El argumento "times" repite un vector completo tantas veces como se le indique.

## ###################################################################
## (2) Calcular un vector telefonos1961 con el número de líneas
## telefónicas en cada zona en el año 1961; nombrar cada dato con la
## zona correspondiente.

# A través de la función "which" conseguimos la posición en el "factor_año" de los teléfonos en 1961, quedando guardados en "año_1961".
año_1961 <- which(factor_año==1961)
año_1961

# Gracias a esta instrucción conseguimos los teléfonos del año 1961.
telefonos1961 = telefonos[año_1961]
telefonos1961

# Para finalizar se le asiga a cada valor el nombre de cada una de las zonas correspondientes.
names(telefonos1961)<- factor_zona[año_1961]
telefonos1961

## ###################################################################
## (3) Calcular el vector porcentaje1961 con el porcentaje que
## representan las líneas telefónicas de cada zona en el año 1961.

prop_1961= (telefonos1961/sum(telefonos1961))*100
prop_1961
# Redondeamos a dos decimales con la función round a 2 decimales.
round(prop_1961,2)

# Ejercicio 5

# El fichero estadisticas_vitales.txt contiene una tabla de datos 
# relativos a las estadísticas vitales genéricas para la población 
# española. La tabla consta de 49 filas, cada una de las cuales se 
# corresponde con un año, desde 1948 hasta 1996. Para cada fila 
# existen 3 columnas que contienen la siguiente información:

# Población total en ese año.
# Número de nacimientos.
# Número de defunciones.

# Se pide construir un marco de datos con el contenido del fichero
# anterior y escribir expresiones de R que seleccionen del mismo la 
# siguiente información:


# Para la lectura de los datos procedemos con la función "read.csv", utilizando:
# 1: "sep" para diferenciar las distintas columnas de la tabla de datos.
# 2: "encoding = "UTF-8"" para reconocer los caracteres como tildes y demás.

# Tomamos el vector "año" para tener la secuencia de todos los años incluidos en los datos.
# Por último utilizamos "attach(datos)" para poder utilizar cada columna de la tabla de forma independiente.

datos <- read.csv("estadisticas_vitales.txt", sep = "", encoding = "UTF-8")
años <- rownames(datos)
attach(datos)

# 1. La población en el año 1992.

# Seleccionamos de la columna "Población" la fila que coincide con "1992".

Población[ años == "1992"]

# 2. Definir una función crecimientoAño que, dado un año, devuelva 
# el creciemiento vegetativo correspondiente a dicho año. En caso 
# de no existir el dato en la tabla, la función debe producir un
# error y mostrar el siguiente mensaje: "Sólo disponibles para los
# años entre 1948 y 1996".

## Definimos la función con la instrucción "function()" y le exigimos 
## el argumento "ano", para el año.


# Denominamos la funcion como crecimientoAño que recibe un atributo año
crecimientoAño <- function(año){
  if(año >= 1948 && año <= 1996){ # La condicional es si año esta en el periodo dado
    Nacimientos[años == año] - Defunciones[años == año] # La solución es los nacimientos en el año introducido en la función
                                                    # menos las defunciones en ese año
  }else{ # Decuelve el siguiente mensaje 
    message("Sólo disponibles para los años entre 1948 y 1996")
  }
}
crecimientoAño(1989) # Probamos la función en el año 1989
crecimientoAño(1996) # en el año 1996
crecimientoAño(2000) # y en el año 2000, fuera del rango

# 3. El número de nacimientos desde el primer hasta el último año.

# Sumamos la columna "Nacimientos" para hallar el total.

sum(Nacimientos)

# 4.La población, el número de nacimientos y el número de defunciones
# del año 1960.

# Seleccionamos los datos donde "años" coincida con "1960".

datos[años == "1960",]

# 5. La población, el número de nacimientos y el número de defunciones
# desde el año 1950 hasta el año 1989.

# Para seleccionar estos datos tomamos como condición que "años" sea mayor que "1950" y menor que "1989",
# seleccionando para este caso todas las columnas.

datos[ años >= "1950" & años <= "1989", ]

# 6.El número de nacimientos y defunciones de los años 1950, 1960,
# 1970, 1980 y 1990.

# Para estos datos, al querer una secuencia con salto 10, hemos utilizado las funciones "seq" y "which",
# para aquellos datos de 1950 a 1990 con salto 10. Por último indicamos que queremos todas las columnas excepto la primera.

datos[seq(which(años == "1950"), which(años == "1990"), 10), -1]
