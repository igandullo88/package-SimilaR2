## ###################################################################
## Ejercicios de vectores, factores y gráficos.
## ###################################################################

## Los datos necesarios para realizar los siguientes ejercicios se
## encuentran en el fichero repaso1.RData.

# Cargamos los datos.
load("repaso1.RData")

## ###################################################################
## Ejercicio 1
## ###################################################################

## El vector telefonos contiene el número de lúíneas telefónicas en
## distintas zonas del mundo y distintos años. Los datos están
## organizados por zonas, y para cada zona, por años.

## Los vectores lugar y fecha contienen la información relativa a las
## zonas y años de los que se tiene información.

## ###################################################################
## (1) Crear un factor con la zona asociada a cada dato del vector
## telefonos, y otro factor con la fecha.

# Creamos un vector con los lugares que corresponden a los telefonos y otro con las fechas.
zona <- rep(lugar, each = 7)
tiempo <- rep(fecha, 7)

# Creamos los factores del enunciado a partir del vector anterior.
factor.zona <- factor(zona)
factor.fecha <- factor(tiempo)
factor.zona
factor.fecha

## Si hacemos lo siguiente, vemos como los vectores creamos corresponden en efecto a los telefonos.
datos <- data.frame(telefonos, lugar = zona, fecha = tiempo)
datos

## ###################################################################
## (2) Calcular un vector telefonos1961 con el número de líneas
## telefónicas en cada zona en el año 1961; nombrar cada dato con la
## zona correspondiente.

# Creamos el vector de los teléfonos en el año 1961, los nombramos con la zona correspondiente.
telefonos1961 <- telefonos[factor.fecha == 1961]
names(telefonos1961) <- factor.zona[factor.fecha == 1961]
telefonos1961

## ###################################################################
## (3) Calcular el vector porcentaje1961 con el porcentaje que
## representan las líneas telefónicas de cada zona en el año 1961.

# Para hallar el vector del enunciado, dividimos el vector del apartado (2) por la suma de los
# valores del dicho vector.
porcentaje1961 <- telefonos1961 / sum(telefonos1961)
porcentaje1961

## A continuación, creamos un diagrama de sectores correspondiente al vector de poncentajes.
pie(porcentaje1961, col = rainbow(7))
## ###################################################################
## Ejercicios de repaso de R.
## ###################################################################

## El fichero estadisticas_vitales.txt contiene una tabla de datos relativos a las estadísticas
## vitales genéricas para la población española. La tabla consta de 49 filas, cada una de las cuales
## se corresponde con un año, desde 1948 hasta 1996. Para cada fila existen 3 columnas que contienen
## la siguiente información:

## Población total en ese año.
## Número de nacimientos.
## Número de defunciones.

# Primero, vamos a cargar los datos. Viendo el conjunto de datos, vemos que están separados por
# espacio y que hay que abrirlo con UTF-8 para que nos salga la tilde de Población.
datos <- read.csv("estadisticas_vitales.txt", sep = "", fileEncoding = "UTF-8")

# Observaciones:
# Como la función read.csv nos una elemento cuya clase es un data.frame, y en la indicación nos dice
# que usemos dicha función, vamos a hacer los ejercicios haciendo consultas al data.frame y no a
# vectores o factores. Si quisieramos hacerlo con vectores o factores, bastaría tomar tres vectores
# que fueran:
# * Población <- datos[, 1].
# * Nacimientos <- datos[, 2].
# * Defunciones <- datos[, 3].
# Y hacer los ejercicios igual sabiendo que hallar el valor de un año concreto n, equivale a hallar
# el valor del (n - 1947)-ésimo de cada vector.

## ###################################################################
# Ejercicio 5

# El fichero estadisticas_vitales.txt contiene una tabla de datos 
# relativos a las estadísticas vitales genéricas para la población 
# española. La tabla consta de 49 filas, cada una de las cuales se 
# corresponde con un año, desde 1948 hasta 1996. Para cada fila 
# existen 3 columnas que contienen la siguiente información:

# Población total en ese año.
# Número de nacimientos.
# Número de defunciones.

# Se pide construir un marco de datos con el contenido del fichero
# anterior y escribir expresiones de R que seleccionen del mismo la 
# siguiente información:
## ###################################################################
# 1. La población en el año 1992.

# Para hallarlo, tenemos en cuenta que las filas son los años y las columnas las variables. Por
# tanto, se obtiene de la siguiente forma.
poblacion_1992 <- datos["1992", "Población"]
poblacion_1992

# Observaciones:
# También se podría haber obternido poniendo el número de la fila y de la columna correspondiente,
# es decir, datos[45, 1] ya que 1992 - 1947 = 45

## ###################################################################
# 2. Definir una función crecimientoAño que, dado un año, devuelva 
# el creciemiento vegetativo correspondiente a dicho año. En caso 
# de no existir el dato en la tabla, la función debe producir un
# error y mostrar el siguiente mensaje: "Sólo disponibles para los
# años entre 1948 y 1996".

# Creamos una función que dado un año, nos de el crecimiento vegetativo, es decir, la diferencia
# entre nacimientos y defunciones.
# Para ello, primero calculamos el número de la fila correspondiente al año al del que se quiere
# calcular el crecimiento vegetativo.
# Como sabemos que tenemos 49 años porque lo dice el enunciado, si el número de la fila resultante
# está entre 0 y 49, podemos calcular sin problemas crecimiento vegetativo. Para ello, calculamos los
# nacimientos, las defunciones y los restamos. Por último, lo imprimimos por pantalla y nos devuelve
# el valor.
# Sin embargo, si el número de la fila correspondiente no está entre 0 y 49, el "if" sería FALSE y
# pasaríamos al "else". En ese caso, se muestra por pantalla el mensaje pedido.
crecimientoAño <- function(n) {
  numero_fila <- n - 1947
  if (numero_fila > 0 && numero_fila < 50) {
    nacimientos <- datos[numero_fila, "Nacimientos"]
    defunciones <- datos[numero_fila, "Defunciones"]
    crecimiento_vegetativo <- nacimientos - defunciones
    return(crecimiento_vegetativo)
  }
  else {
    print("Sólo disponibles para los años entre 1948 y 1996")
  }
}

# Observaciones:
# Estamos suponiendo que nos dan un número al menos entero, ya que sino habría problemas a la hora
# de seleccionar las filas correspondientes.
# Podríamos haberlo hecho con as.character, pero como no la hemos visto en clase, no la uso.
# Podríamos poner en la línea anterior a "return(crecimiento_vegetativo)", lo siguiente para que
# imprimiera por pantalla una frase con más información, pero como no lo pide y a lo mejor lo
# necesario es sólo el número, mejor lo quitamos:
# print(paste("El crecimiento vegetativo del año", n, "es", crecimiento_vegetativo)).

## ###################################################################
# 3. El número de nacimientos desde el primer hasta el último año.

# Calculamos los nacimientos y los sumamos.
nacimientos <- sum(datos[, "Nacimientos"])
nacimientos

# Observaciones:
# Si lo que nos piden es el número de nacimientos en cada año, sería de la siguiente forma; poniendo
# los nombre para que se identifique cada valor:
# nacimientos <- datos[, "Nacimientos"]
# names(nacimientos) <- rownames(datos)
# nacimientos

## ###################################################################
# 4.La población, el número de nacimientos y el número de defunciones
# del año 1960.

# Calculamos todos los datos correspondientes al año 1960, recordando que los años están por filas.
datos_1960 <- datos["1960", ]
datos_1960

# Observaciones:
# Si queremos cada dato por separado, sería de la siguiente forma:
# poblacion_1960 <- datos["1960", "Población"]
# nacimientos_1960 <- datos["1960", "Nacimientos"]
# defunciones_1960 <- datos["1960", "Defunciones"]
# poblacion_1960
# nacimientos_1960
# defunciones_1960

## ###################################################################
# 5. La población, el número de nacimientos y el número de defunciones
# desde el año 1950 hasta el año 1989.

# Calculamos todos los datos desde 1950 hasta 1989. Para ello, tenemos en cuenta que la fila
# correspondiente a 1950 es 1950 - 1947 = 3, y la de 1989 es 1989 - 1947 = 42.
# Finalmente, con la función "seq", obtenemos todos los valores desde 3 a 42.
fila_1950 <- 1950 - 1947
fila_1989 <- 1989 - 1947
numeros_1950_1989 <- seq(fila_1950, fila_1989)
datos_1950_1989 <- datos[numeros_1950_1989, ]
datos_1950_1989

# Observaciones:
# Igual que antes, si se pide por separado, la componente de la fila sería igual y la de la columna
# sería cada una por separado.
# Podríamos haberlo hecho con as.character, pero como no la hemos visto en clase, no la uso.

## ###################################################################
# 6.El número de nacimientos y defunciones de los años 1950, 1960,
# 1970, 1980 y 1990.

# Caculamos los nacimientos y defunciones de los años indicados, usando la función "seq", usando
# ahora la opción "by", que la ponemos cada 10.
# Como los años van de 10 en 10 y no se salta ningún año en los datos, podemos hacerlo como antes.
# Como ya hemos calculado el número correspondiente a la fila 1950, no lo volvemos a calcular.
# Por último, nos piden sólo nacimiento y defunciones, por tanto escribimos "-1" para indicar que no
# nos de la columna de Población.
fila_1990 <- 1990 - 1947
numeros_1950_1990 <- seq(fila_1950, fila_1990, by = 10)
nac_y_def_1950_1990 <- datos[numeros_1950_1990, -1]
nac_y_def_1950_1990

# Observaciones:
# Podríamos haberlo hecho con as.character, pero como no la hemos visto en clase, no la uso.
# También podríamos haberlo hecho poniendo en las filas un vector que fuera:
# c("1950", "1960", "1970", "1980", "1990").
# Igual que antes, si se pide por separado, la componente de la fila sería igual y la de la columna
# sería cada una por separado.
