# ###################################################################
## Ejercicios de vectores, factores y gráficos.
## ###################################################################

## Los datos necesarios para realizar los siguientes ejercicios se
## encuentran en el fichero repaso1.RData.
## ###################################################################

## Ejercicio 1
## ###################################################################
## El vector telefonos contiene el número de lineas telefonicas en
## distintas zonas del mundo y distintos años. Los datos están
## organizados por zonas, y para cada zona, por años.
## Los vectores lugar y fecha contienen la información relativa a las
## zonas y años de los que se tiene información.
## ###################################################################

## (1) Crear un factor con la zona asociada a cada dato del vector
## telefonos, y otro factor con la fecha.

region = factor(lugar, rep(7,7)) # Creo los factores.
tiempo = factor(fecha, 7)



## ###################################################################
## (2) Calcular un vector telefonos1961 con el número de líneas
## telefónicas en cada zona en el año 1961; nombrar cada dato con la
## zona correspondiente.
phone67 = telefonos[which(tiempo == "1961")] # telefonos en 1961. which marca la posicion donde se cumple 
names(phone67) = region[which(tiempo == "1961")] # Se añaden los nombres


## ###################################################################
## (3) Calcular el vector porcentaje1961 con el porcentaje que
## representan las líneas telefónicas de cada zona en el año 1961.



prop.table(phone67) # Probabilidades del vector anterior, con prop.table

######################################################################
# Ejercicio 5
# El fichero estadisticas_vitales.txt contiene una tabla de datos 
# relativos a las estadísticas vitales genéricas para la población 
# española. La tabla consta de 49 filas, cada una de las cuales se 
# corresponde con un año, desde 1948 hasta 1996. Para cada fila 
# existen 3 columnas que contienen la siguiente información:
# Población total en ese año.
# Número de nacimientos.
# Número de defunciones.
# Se pide construir un marco de datos con el contenido del fichero
# anterior y escribir expresiones de R que seleccionen del mismo la 
# siguiente información:

datos <- read.table("Dataset/Sintetico/estadisticas_vitales.txt",fileEncoding = "utf-8")

# 1. La población en el año 1992.

datos["1992", "Poblacion"] # Poblacion (columna) en 1992 (fila), 

# 2. Definir una función crecimientoAño que, dado un año, devuelva 
# el creciemiento vegetativo correspondiente a dicho año. En caso 
# de no existir el dato en la tabla, la función debe producir un
# error y mostrar el siguiente mensaje: "Sólo disponibles para los
# años entre 1948 y 1996".
## Definimos la función con la instrucción "function()" y le exigimos 
## el argumento "ano", para el año.


crecimiento = function( k){
  datos[k, 2] - datos[k,3] # nacimeintos menos defunciones en un año k
}



# 3. El número de nacimientos desde el primer hasta el último año.

sum(datos[,2]) # nacimientos coluna dos, se suman

# 4.La población, el número de nacimientos y el número de defunciones
# del año 1960.

datos["1960",] # fila 1960.


# 5. La población, el número de nacimientos y el número de defunciones
# desde el año 1950 hasta el año 1989.

datos[paste(1950:1989),] # filas de 1950 a 1989 se hace un paste para que se considere texto
                         # tambien, se puede hacer as.character

# 6.El número de nacimientos y defunciones de los años 1950, 1960,
# 1970, 1980 y 1990.

datos[as.character(seq(1950,1990,10)),] # Igual que antes, con as.character

