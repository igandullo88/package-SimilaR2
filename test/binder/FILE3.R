f1= function(){# ###################################################################
## Ejercicios de vectores, factores y graficos.
## ###################################################################
## Los datos necesarios para realizar los siguientes ejercicios se
## encuentran en el fichero repaso1.RData.


load("repaso1.RData")

## ###################################################################
## Ejercicio 1
## ###################################################################
## El vector telefonos contiene el numero de lineas telefonicas en
## distintas zonas del mundo y distintos anos. Los datos estan
## organizados por zonas, y para cada zona, por anos.
## Los vectores lugar y fecha contienen la informacion relativa a las
## zonas y anos de los que se tiene informacion.
## ###################################################################
## (1) Crear un factor con la zona asociada a cada dato del vector
## telefonos, y otro factor con la fecha.
## ###################################################################

# Primero convertimos a factor un vector que tenga varios niveles distintos sin repetir con la funcion "factor".
# Para repetir algun dato de interes utilizamos la funcion "rep".
# Para repetir cada dato de un vector usamos el argumento "each".
# Para repetir un vector entero usamos el argumento "times".

lug_fac=factor(rep(lugar, each= 7))  # Hacemos factor el vector y repetimos cada dato 7 veces.
lug_fac
fec_fac=factor(rep(fecha, times= 7)) # Hacemos factor el vector y repetimos el vector 7 veces.
fec_fac

## (2) Calcular un vector telefonos1961 con el numero de lineas
## telefonicas en cada zona en el ano 1961; nombrar cada dato con la
## zona correspondiente..
## ###################################################################

# Obtenemos la posicion de los telefonos en 1960 con la funcion "which".
fec_1960 <- which(fec_fac==1960)
# Ahora obtenemos los telefonos en 1960 con la siguiente instruccion.
tel_1960 = tel[fec_1960]
# ya nombramos a los telefonos con los lugares y se lo adjudicamos al vector. Tambien nombramos a las ciudades de 1960.
names(tel_1960)<- lug_fac[fec_1960]
tel_1960

## (3) Calcular el vector porcentaje1961 con el porcentaje que
## representan las lineas telefonicas de cada zona en el ano 1961.

# Ahora dividimos por la suma del vector tel_1960 y multiplicamos por 100.
por_1960= (tel_1960/sum(tel_1960)) * 100 
round(por_1960,2) # Con la funcion "round" redondeamos a 2 decimales para ver mejor el resultado.


# Ejercicio 5

# El fichero estadisticas_vitales.txt contiene una tabla de datos 
# relativos a las estadisticas vitales genericas para la poblacion 
# espanola. La tabla consta de 49 filas, cada una de las cuales se 
# corresponde con un ano, desde 1948 hasta 1996. Para cada fila 
# existen 3 columnas que contienen la siguiente informacion:
# Poblacion total en ese ano.
# Numero de nacimientos.
# Numero de defunciones.


# Se pide construir un marco de datos con el contenido del fichero
# anterior y escribir expresiones de R que seleccionen del mismo la 
# siguiente informacion:

## Construimos el marco de datos.
datos <- read.csv("estadisticas_vitales.txt", header = T, 
                  encoding = "UTF-8", sep = "")

## Los datos estan ordenados de la siguiente forma:
## Por filas:
### Desde el year 1948 al year 1996
## Por columnas
### 3 columnas: 1- Poblacion, 2- nacimientos, 3 - Defunciones.

# 1. La poblacion en el ano 1992.

years <- rownames(datos) # Creamos el vector "years" con el nombre de las filas de nuestro conjunto de datos.

# Nos quedamos con las filas del year 1992 y nos quedamos con la poblacion.
pob_92 <- datos[which(years == "1992"), 1]
cat("La poblacion en 1992 es de", pob_92)

# 2. Definir una funcion crecimientoAno que, dado un ano, devuelva 
# el creciemiento vegetativo correspondiente a dicho ano. En caso 
# de no existir el dato en la tabla, la funcion debe producir un
# error y mostrar el siguiente mensaje: "Solo disponibles para los
# anos entre 1948 y 1996".
## Definimos la funcion con la instruccion "function()" y le exigimos 
## el argumento "ano", para el ano.

# Ahora definimos nuestra funcion "crec_veg" y le exigimos un year.
}
crec_veg <- function(ano){
  # Ponemos como condicion que el year este entre 1948 y 1996.
  if (ano >= 1948 & ano <= 1996) {
    # Si pasa la condicion:
    fila <- which(years == ano) # Nos quedamos con el year introducido.
    # Restamos los nacimientos menos las defunciones para conseguir el crecimiento vegetativo.
    crecim_veget = datos[fila,2] - datos[fila,3] 
    cat("El crecimiento vegetativo en el year", ano, "es de",
        crecim_veget)
  }else{
    # En otro caso devpnvemos un error.
    stop("Solo disponibles para los years entre 1948 y 1996")
  }
}
f2= function(){
crec_veg(1992)
crec_veg(1996)
crec_veg(1999)

# 3. El numero de nacimientos desde el primer hasta el ultimo ano.

# Para sumarlo todo el vector de nacimientos cogemos la columna 2 y utilizamos la funcion "sum".
nacim = sum(datos[,2])
cat("El numero de nacimientos entre 1948 y 1996 es de", nacim)

# 4.La poblacion, el numero de nacimientos y el numero de defunciones
# del ano 1960.

# Cogemos todas las columnas pero solo del year 1960.
datos[which(years == "1960"),]

# 5. La poblacion, el numero de nacimientos y el numero de defunciones
# desde el ano 1950 hasta el ano 1989.

ini <- which(years == "1950") # Conseguimos la fila de 1950.
fin <- which(years == "1989") # Conseguimos la fila de 1989.
datos[ini:fin,] # Seleccionamos desde 1950 a 1989 del marco de datos.

# 6.El numero de nacimientos y defunciones de los anos 1950, 1960,
# 1970, 1980 y 1990.

year1950 <- which(years == "1950") # Cogemos la fila del primer year.
datos[seq(year1950, year1950 + 40, 10),-1] }# Tomamos una secuencia de salto 10 para lograr lo que se pide y nos quedamos con los nacimientos y defunciones.
