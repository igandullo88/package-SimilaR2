f1= function(){# ###################################################################
## Ejercicios de vectores, factores y graficos.
## ###################################################################

## Los datos necesarios para realizar los siguientes ejercicios se
## encuentran en el fichero repaso1.RData.

load("repaso1.RData")

## ###################################################################
## Ejercicio 1
## ###################################################################
## El vector telefonos contiene el numero de lineas telefonicas en
## distintas zonas del mundo y distintos anos. Los datos estan
## organizados por zonas, y para cada zona, por anos.
## Los vectores lugar y fecha contienen la informacion relativa a las
## zonas y anos de los que se tiene informacion.

## ###################################################################
## (1) Crear un factor con la zona asociada a cada dato del vector
## telefonos, y otro factor con la fecha.
## ###################################################################

# Con la funcion "factor" transformamos un vector en un factor con tantos niveles como datos tenga, sin repetir.
# Con la funcion "rep", la usamos para repetir algun dato o datos de interes. 
# El argumento "each" de la funcion rep, lo usamos para repetir cada dato de un vector 
# el numero de veces que le digamos. 
# El argumento "times" repite el vector el numero de veces que le digamos.

f_lugar=factor(rep(lugar, each= 7))  # 
f_lugar
f_fecha=factor(rep(fecha, times= 7)) # 
f_fecha

## (2) Calcular un vector telefonos1961 con el numero de lineas
## telefonicas en cada zona en el ano 1961; nombrar cada dato con la
## zona correspondiente.



# Con la funcion which obtenemos la posicion en el f_fecha de los telefonos en 1961
# y lo guardamos en fec_61

fec_61 <- which(f_fecha==1961)

# Con la instruccion "telefonos[fec_61]" obtenemos los telefonos en 1961

tel_61 = telefonos[fec_61]

# Solo queda poner los nombres de los lugares a los telefonos, 
# le adjudicamos nombres al vector con names(tel_61)
# le damos los nombres de las ciudades de los datos de 1961, 
# de la misma forma que antes "f_lugar[fec_61]"

names(tel_61)<-f_lugar[fec_61]
tel_61

## (3) Calcular el vector porcentaje1961 con el porcentaje que
## representan las lineas telefonicas de cada zona en el ano 1961.

# Como ya tenemos el vector tel_61 creado, tan solo tenemos que dividir por la suma
# del vector tel_61 y multiplicarlo por 100.

porc_61=(tel_61/sum(tel_61))*100 
round(porc_61,2) # Redondeamos a dos decimales.


# Ejercicio 5

# El fichero estadisticas_vitales.txt contiene una tabla de datos 
# relativos a las estadisticas vitales genericas para la poblacion 
# espanola. La tabla consta de 49 filas, cada una de las cuales se 
# corresponde con un ano, desde 1948 hasta 1996. Para cada fila 
# existen 3 columnas que contienen la siguiente informacion:
# Poblacion total en ese ano.
# Numero de nacimientos.
# Numero de defunciones.

# Se pide construir un marco de datos con el contenido del fichero
# anterior y escribir expresiones de R que seleccionen del mismo la 
# siguiente informacion:

# Construimos el marco de datos.

datos<-read.csv("estadisticas_vitales.txt",header=T,encoding="UTF-8",sep="")

# Los datos estan ordenados de la siguiente forma:
# Por filas:
## Desde el ano 1948 al ano 1996
# Por columnas
## 3 columnas: 1- Poblacion, 2- Nacimientos, 3 - Defunciones.

# 1. La poblacion en el ano 1992.

agnos <- rownames(datos) # Creamos el vector "agnos" con el nombre de las filas de nuestro marco de datos
# De esta forma  facilitmaos la comprension de las futuras instrucciones.

# Con which obtenemos la fila de los datos en el ano 1992.
# Indicamos que nos devuelva la columna 1 (Poblacion).

pob_92<-datos[which(agnos=="1992"),1]
cat("La poblacion en 1992 es",pob_92)

# 2. Definir una funcion crecimientoAno que, dado un ano, devuelva 
# el creciemiento vegetativo correspondiente a dicho ano. En caso 
# de no existir el dato en la tabla, la funcion debe producir un
# error y mostrar el siguiente mensaje: "Solo disponibles para los
# anos entre 1948 y 1996".
## Definimos la funcion con la instruccion "function()" y le exigimos 
## el argumento "ano", para el ano..

# Definimos la funcion con la instruccion "function()" y le exigimos el argumento "AAA", para el ano.
}
crec_agno <- function(AAA){
  # La primera parte de la funcion es exigirle la condicion de que el ano este entre el 48 y el 96.

   if (AAA >= 1948 & AAA <= 1996) {
    # En ese caso:
    fila <- which(agnos == AAA) # Obtenemos la fila del marco del ano introducido.
    # Para obtener el crecimiento vegetativo le restamos a las defunciones a los nacimientos
   
    crecimientovegetativo=datos[fila,2]-datos[fila,3] 
    
    cat("El crecimiento vegetativo en el ano",AAA,"es de",crecimientovegetativo)
  }else{
    # Si no cumple la condicion, devolvemos un error con la funcionn "stop".
    stop("Solo disponible para los anos entre 1948 y 1996")
  }
}
f2= function(){
# Comprobacion:

crec_agno(1992)
crec_agno(1996)
crec_agno(1999)

# 3. El numero de nacimientos desde el primer hasta el ultimo ano.

# Tenemos que sumar todo el vector de nacimientos. 
# Como los nacimientos estan en la columna 2 del marco de datos, aplicamos la funcion
# "sum" a toda la columna 2 de ese marco.
n_nacim=sum(datos[,2])

cat("El numero de nacimientos entre 1948 y 1996 es de",n_nacim)

# 4.La poblacion, el numero de nacimientos y el numero de defunciones
# del ano 1960.

# Queremos todas las columnas del marco en el ano 1960. 

# Obtenemos la fila del marco en 1960 y en la parte de la columna no ponemos nada.
datos[which(agnos=="1960"),]

# 5. La poblacion, el numero de nacimientos y el numero de defunciones
# desde el ano 1950 hasta el ano 1989.

f_ini<-which(agnos=="1950") # obtenemos la fila del ano de inicio.
f_fin<-which(agnos=="1989") # obtenemos la fila del ano de fin.

datos[f_ini:f_fin,] # Del marco de datos le pedimos los datos desde la fila de inicio
# a la fila final

# 6.El numero de nacimientos y defunciones de los anos 1950, 1960,
# 1970, 1980 y 1990.

agno_50<-which(agnos=="1950") # Obtenemos la fila del ano de inicio
datos[seq(agno_50, agno_50+40,10),-1] # Aplicamos una secuencia de salto 10
# para obtener los datos que nos piden.
# y cogemos solo las columnas de los nacimientos y las defunciones
# Iniciamos la secuencia en la fila "agno_50" y la terminamos
# en "agno_50+40" (que es lo que nos pide el enunciado) y el paso de la secuencia
}# es 10
